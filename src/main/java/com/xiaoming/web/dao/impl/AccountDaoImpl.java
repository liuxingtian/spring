package com.xiaoming.web.dao.impl;


import com.xiaoming.core.annotation.Autowired;
import com.xiaoming.core.annotation.Repository;
import com.xiaoming.core.utils.ConnectionUtils;
import com.xiaoming.web.dao.AccountDao;

import java.sql.Connection;
import java.sql.PreparedStatement;

@Repository
public class AccountDaoImpl implements AccountDao {
    @Autowired
    private ConnectionUtils connectionUtils;

    @Override
    public void updateAccount(String id, int count) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;

        try {
            connection = connectionUtils.getCurrentThreadConn();
            // 执行SQL获取执行结果
            String sql = "update `account` set total = total + ? where id = ?";
            // 获取预处理statement
            preparedStatement = connection.prepareStatement(sql);
            // 设置参数
            preparedStatement.setInt(1, count);
            preparedStatement.setString(2, id);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }
}
