package com.xiaoming.web.service.impl;


import com.xiaoming.core.annotation.Autowired;
import com.xiaoming.core.annotation.Service;
import com.xiaoming.core.annotation.Transactional;
import com.xiaoming.web.dao.impl.AccountDaoImpl;
import com.xiaoming.web.service.AccountService;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {
    @Autowired
    private AccountDaoImpl accountDao;

    @Override
    public void update() throws Exception {
        accountDao.updateAccount("1", -100);
        accountDao.updateAccount("2", 100);
    }

}
