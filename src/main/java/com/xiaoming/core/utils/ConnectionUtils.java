package com.xiaoming.core.utils;

import com.xiaoming.core.annotation.Component;

import java.sql.Connection;
import java.sql.DriverManager;

@Component
public class ConnectionUtils {
    private ThreadLocal<Connection> threadLocal = new ThreadLocal<Connection>();

    /***
     * 从当前线程获取链接
     */
    public Connection getCurrentThreadConn() throws Exception {

        Connection connection = threadLocal.get();
        if (connection == null) {
            // 加载驱动
            Class.forName("com.mysql.jdbc.Driver");
            // 获取连接
            String url = "jdbc:mysql://118.25.134.32/gifts?characterEncoding=utf-8&serverTimezone=GMT%2B8&useSSL=false";
            connection = DriverManager.getConnection(url, "root", "123456");
            threadLocal.set(connection);
        }
        return connection;
    }
}
