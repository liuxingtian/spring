package com.xiaoming.core.utils;

import com.xiaoming.core.annotation.Autowired;
import com.xiaoming.core.annotation.Component;

/**
 * 事务开启、关闭
 */
@Component
public class TransactionManager {
    @Autowired
    private ConnectionUtils connectionUtils;

    // 开启
    public void beginTransaction() throws Exception {
        connectionUtils.getCurrentThreadConn().setAutoCommit(false);
    }

    // 提交
    public void commit() throws Exception {
        connectionUtils.getCurrentThreadConn().commit();
    }

    // 回滚
    public void rollback() throws Exception {
        connectionUtils.getCurrentThreadConn().rollback();
    }
}
