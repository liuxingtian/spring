package com.xiaoming.core.factory;

import com.xiaoming.core.utils.ClassUtils;

import java.util.HashMap;
import java.util.Map;

public class BeanFactory {
    private static Map<String, Object> map = new HashMap<String, Object>();

    static {
        map = new ClassUtils().loadClass("com.xiaoming");
    }

    public static Object getBean(String id) {
        return map.get(id);
    }
}
