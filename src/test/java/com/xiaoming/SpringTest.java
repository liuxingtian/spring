package com.xiaoming;

import com.xiaoming.core.factory.BeanFactory;
import com.xiaoming.core.factory.ProxyFactory;
import com.xiaoming.web.service.AccountService;
import org.junit.jupiter.api.Test;

public class SpringTest {
    // 测试注解
    @Test
    public void testAnnotation() throws Exception {
        AccountService accountService = (AccountService) BeanFactory.getBean("AccountServiceImpl");
        accountService.update();
    }

    // 测试事务
    @Test
    public void testTransactional() throws Exception {
        ProxyFactory proxyFactory = (ProxyFactory) BeanFactory.getBean("ProxyFactory");
        AccountService accountService = (AccountService) proxyFactory.getJdkProxy(BeanFactory.getBean("AccountServiceImpl"));
        accountService.update();
    }
}
